# Base postgres image
FROM postgres:9.4

MAINTAINER Chesley Brown <chesley@bluedrop.com>

# Database credentials
ENV POSTGRES_USER blnapi
ENV POSTGRES_PASSWORD blnapi

# Set permissions for postgres
ADD pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf

# Set postgres config
ADD postgresql.conf /etc/postgresql/9.4/main/postgresql.conf

# Fix Permissions
RUN chown postgres -R /etc/postgresql/9.4/main && chgrp -R postgres /etc/postgresql/9.4/main

# Add postgres setup & start script
ADD start.sh /usr/local/bin/start.sh
RUN chmod +x /usr/local/bin/start.sh

# Expose postgres port
EXPOSE 5432

# Setup & Start postgres
CMD ["start.sh"]