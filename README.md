# BLN API Database Container #

This is the Dockerfile for building a bln-api-db running postgres for dev.

### Requirements ###

* [Docker](https://docs.docker.com/installation/#installation)

### How do I run this container? ###

1. Don't check out this repo, you don't need it.
1. Install [Docker](https://docs.docker.com/installation/#installation) if you haven't already
1. One one of the following commands based on what version of Postgres you want:
    - Run `docker run --name bln-api-db -d -p 5432:5432 coursepark/bln-api-db:9.3` for Postgres 9.3
    - Run `docker run --name bln-api-db -d -p 5432:5432 coursepark/bln-api-db:9.4` for Postgres 9.4
1. If all is well you should see your container running by running `docker ps`
1. Done.

### How do I create a fresh build? ###

You will only need to do a fresh build if you're changing the image. This would not be necessary if you're just using the container.

1. `cd` into the directory
1. `docker build -t bln-api-db .`
1. `docker run -d -p 5432:5432 bln-api-db`