#!/bin/bash
set -e

POSTGRESQL_BIN=/usr/lib/postgresql/9.4/bin/postgres
POSTGRESQL_CONFIG_FILE=/etc/postgresql/9.4/main/postgresql.conf

# fix permissions
chown -R postgres "$PGDATA"

# initalize database only if it hasn't already be initalized
if [ -z "$(ls -A "$PGDATA")" ]; then
	gosu postgres initdb
	
	# setup user account and database for user
	POSTGRESQL_SINGLE="gosu postgres $POSTGRESQL_BIN --single --config-file=$POSTGRESQL_CONFIG_FILE"
	$POSTGRESQL_SINGLE <<< "CREATE USER $POSTGRES_USER WITH PASSWORD '$POSTGRES_PASSWORD';" > /dev/null
	$POSTGRESQL_SINGLE <<< "CREATE DATABASE $POSTGRES_USER OWNER $POSTGRES_USER;" > /dev/null
fi

# start postgres
exec gosu postgres $POSTGRESQL_BIN --config-file=$POSTGRESQL_CONFIG_FILE